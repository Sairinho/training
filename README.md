# Learning Angular, typescript and other things 👨‍💻

Hello guys, This repository has been made so that I can track my progress of **learning to be a good developer** from a basic level to a bit of advanced level. I will be pushing all the knowledge that I have on various topics of the framework with the code that I have written which might be in the form of some fun and interactive projects ✌.

> `Consistency is more important that perfection ✊`


## Resources

Some Resources i am using right now to learn

[Angular Animations](https://angular.io/guide/animations)

[Angular CLI](https://cli.angular.io/)

[Angular Blog](https://blog.angular.io/)

[Learn Angular](https://angular.io/tutorial)
